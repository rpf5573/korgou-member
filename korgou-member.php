<?php

/**
 * Plugin Name: Korgou Member
 * Plugin URI:  https://shoplic.kr/item/korgou-member/
 * Description: Korgou Member Plugin
 * Version:     0.1
 * Author:      Shoplic Inc.
 * Author URI:  https://shoplic.kr
 * License:     Shoplic License
 */

if (!defined('ABSPATH')) {
  exit;
}

define('SHOPLIC_MEMBER_MAIN', __FILE__);
define('SHOPLIC_MEMBER_VERSION', '1.3.2');

if (class_exists('Shoplic_Plugin')) {
  (new Shoplic_Plugin(
    [
      'id'            => 'shoplic_member',
      'version'       => SHOPLIC_MEMBER_VERSION,
      'file'          => SHOPLIC_MEMBER_MAIN,
      'settings_link' => 'users.php?page=shoplic_member',
    ]
  ))->load();
}

if (defined('WP_CLI') && WP_CLI && class_exists('WP_CLI')) {
  try {
    require_once __DIR__ . '/cli.php';
    WP_CLI::add_command('shoplic-member', 'Shoplic_Member_CLI');
  } catch (Exception $e) {
    echo 'WP CLI Error: ' . $e->getMessage();
  }
}
