<?php
defined('ABSPATH') or exit; // Exit if accessed directly

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = '';

    function load()
    {
        $this->add_filter('authenticate', 30, 3);
    }

    function authenticate($user, $username, $password)
    {
        if ($user instanceof WP_User)
            return $user;

        $korgou_user = apply_filters('korgou_user_get_by_email', null, $username);
        if ($korgou_user != null && $korgou_user->check_password($password)) {
            $user = get_user_by('login', $korgou_user->userid);
            wp_set_password($password, $user->ID);
        }

        return $user;
    }
};
