<?php

namespace shoplic;

if (!defined('ABSPATH')) exit; // Exit if accessed directly

class Controller extends \Shoplic_Controller
{
  protected $id = 'shoplic';

  function __construct($file)
  {
    parent::__construct($file);
  }

  function load()
  {
    $this->add_action('register_form_end');
    $this->add_action('profile');
    $this->add_action('password');
    if (!is_admin()) {
      $this->style('front', ['deps' => 'woocommerce-layout']);
      $this->add_shortcode('login');
      $this->add_shortcode('register');
      $this->add_shortcode('profile');
      $this->add_shortcode('password');
    }
  }

  function login()
  {
    if (defined('REST_REQUEST') && REST_REQUEST)
      return;

    if (is_user_logged_in()) {
      $context = [
        'user' => wp_get_current_user(),
      ];
      $this->view('membership', $context);
    } else {
      $messages = [];
      foreach (wc_get_notices('error') as $error) {
        if (is_string($error))
          $messages[] = $error;

        if (is_array($error) && isset($error['notice']))
          $messages[] = $error['notice'];
      }

      $context = [
        'redirect' => isset($_GET['redirect_to']) ? $_GET['redirect_to'] : '',
        'hidden' => false,
        'message' => implode('<br>', $messages),
      ];

      $this->view('login', ['context' => $context]);
    }
  }

  function register()
  {
    if (defined('REST_REQUEST') && REST_REQUEST)
      return;

    wp_enqueue_script('jquery-form');
    do_action('shoplic_enqueue', 'postcode');
    do_action('shoplic_enqueue', 'inputmask');
    do_action('shoplic_enqueue', 'jquery-validate');
    $this->script('register');

    $option = $this->get_option('shoplic_member_register_option');

    $fields = apply_filters('shoplic_register_fields', apply_filters('shoplic_member_fields', []));
    $context = [
      'fields' => $fields,
      // 'terms_content' => ($option->terms !== NULL) ? apply_filters('the_content', get_post_field('post_content', $option->terms, 'raw')) : '',
      'terms_content' => ($option->terms !== NULL) ? \Shoplic_Util::get_post_content($option->terms) : '',
      'privacy_agree_content' => ($option->privacy_agree  !== NULL) ? \Shoplic_Util::get_post_content($option->privacy_agree) : '',
      'option' => $this->get_option('shoplic_member_register_option')
    ];

    $this->view('register', $context);
  }

  function profile()
  {
    if (defined('REST_REQUEST') && REST_REQUEST)
      return;

    wp_enqueue_script('jquery-form');
    do_action('shoplic_enqueue', 'postcode');
    do_action('shoplic_enqueue', 'inputmask');
    do_action('shoplic_enqueue', 'jquery-validate');
    $this->script('profile');

    $fields = apply_filters('shoplic_member_fields', []);
    $context = [
      'fields' => $fields,
    ];

    $this->template('profile', $context);
  }

  function password()
  {
    if (defined('REST_REQUEST') && REST_REQUEST)
      return;

    wp_enqueue_script('jquery-form');
    do_action('shoplic_enqueue', 'jquery-validate');

    $this->script('password');

    $this->template('password');
  }

  function input($section, $key, $field)
  {
    $type = $field['type'];
    $field['section'] = $section;
    if ($section == 'profile') {
      if ($key == 'user_login' || $key == 'user_email' || $key == 'first_name') {
        $user = wp_get_current_user();
        $value = $user->$key;
      } else {
        $user_id = get_current_user_id();
        $value = get_user_meta($user_id, $key, true);
      }
      $field['value'] = $value;
    }

    $field = wp_parse_args($field, [
      'name' => $key,
      'input_class' => 'input-' . (in_array($type, ['email', 'text', 'password', 'postcode']) ? 'text' : $type)
    ]);
    // echo \Shoplic_Form::input($field, true);
    \Shoplic_Form::$type($field);
  }

  function input_agreement($key, $label, $content)
  {
    $context = compact('key', 'label', 'content');
    $this->view('input_agreement', $context);
  }

  function register_form_end()
  {
    $option = $this->get_option('shoplic_member_register_option');
    if ($option->recaptcha == 'v3' && $option->recaptcha_site_key != '' && $option->recaptcha_secret_key != '') {
      $this->view('grecaptcha_v3', ['site_key' => $option->recaptcha_site_key]);
    }
  }
};

return new Controller(__FILE__);
