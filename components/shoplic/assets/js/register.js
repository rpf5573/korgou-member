jQuery(function($) {
    $.extend($.validator.messages, {
        required: '필수입력 항목입니다.',
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
    var validator = $('.shoplic-register form').validate({
        rules: {
            confirm_pass: {
                equalTo: '#sf-register-user_pass'
            },
            user_email: {
                email: true
            }
        },
        messages: {
            confirm_pass: {
                equalTo: '비밀번호가 일치하지 않습니다.'
            },
            user_email: {
                email: '유효한 이메일 주소를 입력하십시오.'
            }
        }
    });

    $('.shoplic-register form').ajaxForm({
        beforeSubmit: function(arr, $form, options) {
            var agreement = true;
            $('.register-agreement').each(function() {
                if (!$(this).is(':checked')) {
                    alert($(this).data('field')+'에 동의해주십시오.');
                    $(this).focus();
                    agreement = false;
                    return false;
                }
            })
            if (!agreement)
                return false;
            
            return true;
        },
        success: function(response) {
            if (response.success) {
                location.href = response.data;
            } else {
                validator.showErrors(response.data);
            }
        }
    });

    // $('#shoplic-register-btn').on('click.register', function() {
    //     $('.shoplic-register form').submit();
    //     return false;
    // });
});
