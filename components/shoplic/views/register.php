<div class="woocommerce shoplic-register">
    <form method="post" class="woocommerce-form woocommerce-form-register shoplic-form register" action="<?php echo admin_url( 'admin-ajax.php' ); ?>">

        <?php do_action( 'shoplic_register_form_start' ); ?>

        <input type="hidden" name="action" value="shoplic_member_register">
    	<?php wp_nonce_field('shoplic_member_register'); ?>
        <section class="agreement-section">
            <h3>약관 동의</h3>

            <?php if (!empty($terms_content)) $this->input_agreement('terms', '이용약관', $terms_content); ?>

            <?php if (!empty($privacy_agree_content)) $this->input_agreement('privacy_agree', '개인정보 수집 및 이용', $privacy_agree_content); ?>
        </section>

        <section class="user-fields-section">
            <h3>회원 정보</h3>

        	<?php do_action( 'woocommerce_register_form_start' ); ?>

            <?php foreach ($fields as $key => $field): ?>
                <?php if (!isset($field['enabled'])) continue; ?>
                <?php if ($field['type'] == 'postcode'): ?>
                    <p class="form-row form-row-first">
                        <?php $this->input('register', $key, $field); ?>
                    </p>
                    <?php echo apply_filters('shoplic_postcode_search_btn', '', 'sf-register-billing'); ?>
                <?php else: ?>
                    <p class="form-row form-row-wide">
                        <?php $this->input('register', $key, $field); ?>
                    </p>
                <?php endif; ?>
        	<?php endforeach; ?>

        	<?php do_action( 'woocommerce_register_form' ); ?>

        </section>

    	<p class="form-row" style="margin-top: 2em;">
            <abbr class="required" title="필수">*</abbr> 필수입력 항목입니다.
    	</p>

    	<p class="form-row" style="margin-top: 2em;">
    		<button type="submit" id="shoplic-register-btn" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
    	</p>

    	<?php do_action( 'woocommerce_register_form_end' ); ?>

        <?php do_action('shoplic_register_form_end'); ?>

    </form>

</div>
