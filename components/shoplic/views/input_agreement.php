<div>
    <label for="sf-<?php echo $key; ?>"><?php echo $label; ?></label>
    <div class="agreement-content"><?php echo $content; ?></div>
    <p class="form-row form-row-wide">
      <label for="sf-<?php echo $key; ?>" style="font-weight: normal;">
          <input type="checkbox" id="sf-<?php echo $key; ?>" class="checkbox register-agreement" data-field="<?php echo $label; ?>"> <?php echo $label; ?>에 동의합니다.
          &nbsp;<abbr class="required" title="필수">*</abbr>
      </label>
    </p>
</div>
