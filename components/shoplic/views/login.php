<div class="woocommerce shoplic-login">
  <?php
  $verify_email_confirm = isset($_GET['verify_email_confirm']) ? $_GET['verify_email_confirm'] : '';
  if ($verify_email_confirm) { ?>
    <div class="verify-email-confirm-message">
      Your email verification has been completed. Please log in!
    </div>
  <?php
  }

  wc_get_template('global/form-login.php', $context);
  ?>
</div>
