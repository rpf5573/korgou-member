<?php
$field = [
    'type' => 'password'
];
?>
<div class="woocommerce shoplic-password">
    <form method="post" class="woocommerce-form woocommerce-form-password shoplic-form" action="<?php echo admin_url( 'admin-ajax.php' ); ?>">
        <input type="hidden" name="action" value="shoplic_member_password">
    	<?php wp_nonce_field('shoplic_member_password'); ?>
        <section class="user-fields-section">
            <p class="form-row form-row-wide">
                <?php $controller->input('password', 'old_pass', array_merge($field, ['id'=>'old_pass', 'label'=>'현재 비밀번호'])); ?>
            </p>
            <br>
            <p class="form-row form-row-wide">
                <?php $controller->input('password', 'user_pass', array_merge($field, ['id'=>'user_pass', 'label'=>'새 비밀번호'])); ?>
            </p>
            <p class="form-row form-row-wide">
                <?php $controller->input('password', 'confirm_pass', array_merge($field, ['id'=>'confirm_pass', 'label'=>'새 비밀번호 확인'])); ?>
            </p>
        </section>

    	<p class="form-row" style="margin-top: 2em;">
            <button type="submit" id="shoplic-password-btn" class="woocommerce-Button button" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
    	</p>

    	<?php do_action( 'woocommerce_password_form_end' ); ?>
    </form>
</div>
