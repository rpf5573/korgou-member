<input type="hidden" name="recaptcha_token">
<script src="https://www.google.com/recaptcha/api.js?render=<?php echo $site_key; ?>"></script>
<script>
/*
grecaptcha.ready(function() {
    grecaptcha.execute('<?php echo $site_key; ?>', {action: 'register'}).then(function(token) {
        jQuery('input[name="recaptcha_token"]').val(token);
    });
});
*/
jQuery(function($) {
    $('#shoplic-register-btn').on('click.register', function() {
        grecaptcha.execute('<?php echo $site_key; ?>', {action: 'register'}).then(function(token) {
            $('input[name="recaptcha_token"]').val(token);
            console.log(token);
            $('.shoplic-register form').submit();
        });
        return false;
    });
})
</script>
