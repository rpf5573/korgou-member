<div class="woocommerce shoplic-profile">
    <form method="post" class="woocommerce-form woocommerce-form-register shoplic-form" action="<?php echo admin_url( 'admin-ajax.php' ); ?>">
        <input type="hidden" name="action" value="shoplic_member_profile">
    	<?php wp_nonce_field('shoplic_member_profile'); ?>
        <section class="user-fields-section">
            <?php foreach ($fields as $key => $field):
                if (isset($field['modifiable']) && $field['modifiable'] == 'false') {
                    $field['readonly'] = true;
                } 
                if (!isset($field['enabled']) || $field['type'] == 'password') continue;
                if ($field['type'] == 'postcode'): ?>
                    <p class="form-row form-row-first">
                <?php else: ?>
                    <p class="form-row form-row-wide">
                <?php endif; ?>

                <?php $controller->input('profile', $key, $field); ?>

                <?php if ($field['type'] == 'postcode'): ?>
                    </p>
                    <?php echo apply_filters('shoplic_postcode_search_btn', '', 'sf-profile-billing'); ?>
                <?php else: ?>
                    </p>
                <?php endif; ?>
        	<?php endforeach; ?>

        </section>

    	<p class="form-row" style="margin-top: 2em;">
            <abbr class="required" title="필수">*</abbr> 필수입력 항목입니다.
    	</p>

    	<p class="form-row" style="margin-top: 2em;">
    		<button type="submit" id="shoplic-profile-btn" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
    	</p>

    	<?php do_action( 'woocommerce_register_form_end' ); ?>

    </form>

</div>
