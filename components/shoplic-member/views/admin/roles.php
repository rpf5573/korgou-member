<?php
/**
 * Context:
 *
 * @var array  $roles
 * @var string $base_id
 * @var string $base_name
 * @var array  $value
 * @var string $desc
 */
?>
<ul id="<?php echo esc_attr( $base_name ); ?>" style="margin: 0;">
	<?php foreach ( $roles as $key => $name ) : ?>
		<li>
			<input type="checkbox"
				   id="<?php echo esc_attr( "{$base_id}-{$key}" ); ?>"
				   name="<?php echo esc_attr( $base_name ); ?>[]"
				   value="<?php echo esc_attr( $key ); ?>"
				<?php checked( in_array( $key, $value ) ); ?>
			>
			<label for="<?php echo esc_attr( "{$base_id}-{$key}" ); ?>"><?php echo esc_html( $name ); ?></label>
		</li>
	<?php endforeach; ?>
</ul>
<span class="description"><?php echo esc_html( $desc ); ?></span>
