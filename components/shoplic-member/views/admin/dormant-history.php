<?php
/**
 * Context:
 *
 * @var array  $rows
 *
 * @var string $desc
 */
$props = [ 'notified_success', 'notified_error', 'deactivated_success', 'deactivated_error' ];
?>
<table class="widefat striped shoplic-member-dormant-history">
	<thead class="thead-light">
	<tr>
		<th>작업 일시</th>
		<th>사전 통지 성공</th>
		<th>사전 통지 실해</th>
		<th>휴면 처리 성공</th>
		<th>휴면 처리 실패</th>
	</tr>
	</thead>
	<tbody>
	<?php if ( ! empty( $rows ) ) : ?>
		<?php foreach ( $rows as $row ) : ?>
			<tr>
				<td>
					<?php echo esc_html( $row->created->format( 'Y-m-d H:i:s' ) ); ?>
				</td>
				<?php foreach ( $props as $prop ) : ?>
					<td>
						총 <?php echo (string) sizeof( $row->{$prop} ); ?>명
						<?php if ( sizeof( $row->{$prop} ) > 0 ): ?>
							[<a href="#"
								data-label-collapse="접기"
								data-label-expand="보기"
								class="user-list-gutter">보기</a>]
							<ul style="display: none;">
								<?php foreach ( $row->{$prop} as $user_id ): ?>
									<li>
										<?php
										$user       = get_userdata( $user_id );
										$user_email = $user ? $user->user_email : '';
										$edit_link  = $user ? get_edit_user_link( $user->ID ) : '#';
										?>
										<a href="<?php echo esc_url( $edit_link ); ?>" target="_blank">
											<?php echo esc_html( $user_email ); ?> (#<?php echo $user_id; ?>)
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; ?>
	<?php else : ?>
		<tr>
			<td colspan="4">내역이 존재하지 않습니다.</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
<span class="description"><?php echo esc_html( $desc ); ?></span>

<script>
	(function ($) {
		$('table.shoplic-member-dormant-history').find('a.user-list-gutter').on('click', function (e) {
			var elem = $(this);
			e.preventDefault();
			elem.siblings('ul').slideToggle(200);
			if (elem.text() === elem.data('labelCollapse')) {
				elem.text(elem.data('labelExpand'));
			} else {
				elem.text(elem.data('labelCollapse'));
			}
		});
	})(jQuery);
</script>

<style>
	table.shoplic-member-dormant-history {
		margin-bottom: 10px;
	}

	table.shoplic-member-dormant-history th {
		padding-left: 10px;
	}
</style>