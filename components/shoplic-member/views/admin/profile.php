<?php
/**
 * Context:
 *
 * @var $fields
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! isset( $fields ) || empty( $fields ) ) {
	return;
}
?>
<h2>회원 정보</h2>
<table class="form-table shoplic-member" role="presentation">
	<?php foreach ( $fields as $key => $field ) : ?>
		<tr class="<?php echo sanitize_html_class( $key ); ?>">
			<th>
				<?php echo wp_kses( $field['label'] ?? '', [ 'label' => [ 'for' => true ] ] ); ?>
			</th>
			<td>
				<?php echo $field['html'] ?? ''; ?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
