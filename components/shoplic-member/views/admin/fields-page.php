<?php
/**
 * Context:
 *
 * @var array  $fields  필드 정보
 * @var string $section 현재 출력 중인 섹션
 */
?>
<table class="widefat dbck-table shoplic-user-field-table">
	<colgroup>
		<col style="width: 34px;">
		<col style="width: 60px;">
		<col>
		<col>
		<col>
		<col style="width: 60px;">
	</colgroup>
	<thead class="thead-light">
	<tr>
		<th></th>
		<th>사용</th>
		<th>필드</th>
		<th>필드 레이블</th>
		<th>필드 설명</th>
		<th>필수</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ( $fields as $id => $field ): ?>
		<tr>
			<td class="sort ui-sortable-handle" style="vertical-align: middle !important;">
				<?php echo Shoplic_Settings_Field::input_hidden(
					[
						'section' => $section,
						'id'      => $id,
						'key'     => 'id',
						'value'   => $id,
					]
				); ?>
			</td>
			<td style="text-align:center;">
				<?php echo Shoplic_Settings_Field::input_checkbox(
					[
						'section'  => $section,
						'id'       => $id,
						'key'      => 'enabled',
						'value'    => isset( $field['enabled'] ) ? $field['enabled'] : '',
						'readonly' => isset( $field['must_be_enabled'] ) && $field['must_be_enabled'],
					]
				); ?>
			</td>
			<td><?php echo $field['title']; ?></td>
			<td>
				<?php echo Shoplic_Settings_Field::input_text(
					[
						'section' => $section,
						'id'      => $id,
						'key'     => 'label',
						'value'   => $field['label'],
					]
				); ?>
			</td>
			<td>
				<?php echo Shoplic_Settings_Field::input_text(
					[
						'section' => $section,
						'id'      => $id,
						'key'     => 'placeholder',
						'value'   => isset( $field['placeholder'] ) ? $field['placeholder'] : '',
					]
				); ?>
			</td>
			<td style="text-align:center;">
				<?php echo Shoplic_Settings_Field::input_checkbox(
					[
						'section'  => $section,
						'id'       => $id,
						'key'      => 'required',
						'value'    => isset( $field['required'] ) ? $field['required'] : '',
						'readonly' => isset( $field['must_be_required'] ) && $field['must_be_required'],
					]
				); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
