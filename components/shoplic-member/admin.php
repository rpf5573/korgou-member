<?php

namespace shoplic_member;

use Shoplic_Form;

use WP_User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class Admin extends \Shoplic_Admin {
	protected $id = 'shoplic_member';

	function __construct( $file ) {
		parent::__construct( $file );
	}

	function init() {
		$this->load_page( 'page' );
		$this->add_action( 'fields_option_section' );
		add_action( 'current_screen', [ $this, 'current_screen' ] );

		add_action( 'show_user_profile', [ $this, 'member_profile' ] );
		add_action( 'edit_user_profile', [ $this, 'member_profile' ] );
		add_action( 'personal_options_update', [ $this, 'member_update' ] );
		add_action( 'edit_user_profile_update', [ $this, 'member_update' ] );

		add_action( 'wp_ajax_request_dormant_mail_test', [ $this, 'response_dormant_mail_test' ] );


		$this->add_action( 'dormant_page', 11 );                     // 페이지에 필요한 스크립트 삽입
		$this->add_action( 'dormant_option_field_roles' );           // "대상 역할" 필드 위젯 출력
		$this->add_action( 'dormant_option_field_dormant_history' ); // "휴면 처리 기록" 필드 위젯 출력
	}

	function current_screen( $screen ) {
		if ( $screen->id == 'users_page_shoplic_member' ) {
			do_action( 'shoplic_admin_enqueue', 'select2' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			$this->script( 'admin' );
			$this->style( 'admin' );
		}
	}

	function fields_option_section() {
		$context = [
			'section' => $this->id . '_fields_option',
			'fields'  => $this->apply_filters( 'fields', [] ),
		];
		$this->view( 'fields-page', $context );
	}

	/**
	 * 회원 정보 필드 증강
	 *
	 * @callback
	 * @action      show_user_profile
	 * @action      edit_user_profile
	 *
	 * @param WP_User $profile
	 *
	 * @see         wp-admin/user-edit.php
	 */
	public function member_profile( $profile ) {
		$fields = [];

		foreach ( $this->get_fields() as $key => $field ) {
			if ( filter_var( $elem['enabled'] ?? false, FILTER_VALIDATE_BOOLEAN ) ) {
				continue;
			}
			$fields[ $key ] = $this->convert_field_for_form_table( $profile->ID, $field );
		}

		$this->view( 'profile', [ 'fields' => &$fields ] );

		do_action( 'shoplic_enqueue', 'postcode' );
	}

	/**
	 * 회원 정보 업데이트 콜백
	 *
	 * @callback
	 * @action      personal_options_update
	 * @action      edit_user_profile_update
	 *
	 * @param int $user_id
	 *
	 * @see         wp-admin/user-edit.php
	 */
	public function member_update( $user_id ) {
		$user = get_userdata( $user_id );
		if ( ! $user ) {
			return;
		}

		foreach ( $fields = $this->get_fields() as $field ) {
			if ( filter_var( $elem['enabled'] ?? false, FILTER_VALIDATE_BOOLEAN ) ) {
				continue;
			}
			$name = $field['id'] ?? '';
			if ( $name && isset( $_POST[ $name ] ) ) {
				update_user_meta( $user_id, $name, sanitize_text_field( $_POST[ $name ] ) );
			}
		}
	}

	/**
	 * 휴면 회원 탭에 필요한 스크립트 삽입
	 */
	public function dormant_page() {
		$this->script( 'dormant-page' );
		wp_localize_script(
			$this->get_asset_tag( 'dormant-page' ),
			'dormantPage',
			[
				'nonce'           => wp_create_nonce( $this->get_tag( 'dormant-page' ) ),
				'textConfirmSend' => '테스트 메일을 보내시겠습니까?',
			]
		);

		wp_enqueue_style(
			$this->get_asset_tag( 'dormant-page' ),
			$this->get_url( 'assets/css/dormant-page.css' ),
			[],
			$this->get_version()
		);
	}

	/**
	 * 대상 역할 필드 출력
	 *
	 * @callback
	 * @action      {$args['section']}_field_{$args['id']}
	 *
	 * @param array $args
	 *
	 * @see Shoplic_settings::add_sections()
	 */
	public function dormant_option_field_roles( $args ) {
		$roles    = [];
		$wp_roles = wp_roles();
		foreach ( $wp_roles->roles as $key => $array ) {
			$trans = translate_user_role( $array['name'] );
			if ( $array['name'] === $trans ) {
				$trans = translate_user_role( $array['name'], 'woocommerce' );
			}
			$roles[ $key ] = $trans;
		}
		unset( $roles['administrator'] );
		unset( $roles['deactivated'] );

		$option = get_option( $args['section'], [] );
		$value  = $option['roles'] ?? [];

		$this->view(
			'roles',
			[
				'roles'     => &$roles,
				'value'     => &$value,
				'base_id'   => "{$args['section']}-{$args['id']}",
				'base_name' => $args['label_for'],
				'desc'      => $args['desc'],
			]
		);
	}

	public function dormant_option_field_dormant_history( $args ) {

		/**
		 * @noinspection PhpIncludeInspection
		 * @var Dormant_User $dormant_users
		 */
		$dormant_users = require dirname( $this->file ) . '/includes/dormant-users.php';

		$this->view(
			'dormant-history',
			[
				'rows' => $dormant_users->get_history(),
				'desc' => $args['desc'],
			]
		);
	}

	/**
	 * HTML 출력 보조 함수의 출력 내용을 편집한다.
	 *
	 * @param int   $user_id
	 * @param array $field
	 *
	 * @return array 성공시 label, html 두 키를 가진 배열. 혹은 에러가 있다면 빈 배열.
	 */
	private function convert_field_for_form_table( $user_id, array $field ) {
		$input_types = [ 'email', 'text', 'password', 'postcode' ];

		$type  = $field['type'] ?? '';
		$name  = $field['id'] ?? '';
		$field = wp_parse_args(
			$field,
			[
				'section'     => 'profile',
				'name'        => $name,
				'input_class' => 'input-' . ( in_array( $type, $input_types ) ? 'text' : $type ),
				'value'       => get_user_meta( $user_id, $name, true ),
			]
		);

		/** @see */
		if ( ! is_callable( [ Shoplic_Form::class, $type ] ) ) {
			return [];
		}

		// 템플릿 메소드 호출
		ob_start();
		call_user_func( [ Shoplic_Form::class, $type ], $field );
		$output = ob_get_clean();

		// $output 은 <label>...</label> 이후 HTML 코드가 같이 출력됨.
		// <label> 태그를 추려냄.
		$label_pos = strpos( $output, '</label>' );
		if ( false === $label_pos ) {
			return [];
		}

		$label = substr( $output, 0, $label_pos + 8 );
		$html  = substr( $output, $label_pos + 8 );

		// 우편번호 버튼 추가
		if ( 'postcode' === $type ) {
			/** @see \shoplic_core\shoplic\Controller::postcode_search_btn() */
			$html .= apply_filters( 'shoplic_postcode_search_btn', '', 'sf-profile-billing' );

			// 관리자 화면용 클래스 추가 및 변경
			$html = str_replace(
				'class="woocommerce-input-wrapper"',
				'class="woocommerce woocommerce-input-wrapper"',
				$html
			);

			// 자바스크립트가 동작하도록 클래스 수정
			$html = str_replace(
				'class="postcode-search-btn"',
				'class="postcode-search-btn button button-secondary"',
				$html
			);

			// 버튼 스타일 수정
			$html .= <<<EOD
				<style>
				p#sf-profile-billing_postcode_search_field {
					display: inline-block;
					width: 100px;
					height: 25px;
					margin-top:0;
				}			
				p#sf-profile-billing_postcode_search_field > label {
					display: none;
				}
				</style>
EOD;
		}

		return [
			'label' => $label,
			'html'  => $html,
		];
	}

	/**
	 * 관리자 화면의 프로필 (혹은 사용자 수정) 페이지에 출력할 필드를 가져온다.
	 *
	 * @return array
	 */
	private function get_fields() {
		$excludes = [
			'user_email'   => 0,
			'user_login'   => 0,
			'user_pass'    => 0,
			'confirm_pass' => 0,
			'first_name'   => 0,
		];

		return array_diff_key( $this->apply_filters( 'fields', [] ), $excludes );
	}
}

return new Admin( __FILE__ );
