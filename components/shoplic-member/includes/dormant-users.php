<?php

namespace shoplic_member;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use WP_User;
use WP_Error;
use WP_User_Query;

if ( ! class_exists( 'shoplic_member\Dormant_User' ) ) :

	/**
	 * Class Dormant_User
	 *
	 * 휴면 회원 기능 클래스.
	 * 멀티사이트는 지원하지 않는다.
	 */
	class Dormant_User {
		/** @var int 휴면회원 기준 기본 개월 수 */
		const DORMANT_LIMIT_MONTH = 12;

		/** @var string 크론에 등록된 액션 이름 */
		const DORMANT_ACTION = 'shoplic_member_dormant_user';

		/** @var array 메일 전송시 본문과 제목에서 허용하는 치환 코드 */
		private $magic_codes = [
			'%{SITE_NAME}%',
			'%{LIMIT_MONTH}%',
			'%{LOGIN_LINK}%',
			'%{LOST_LINK}%',
			'%{SITE_URL}%',
			'%{HOME_URL}%',
			'%{DUE_DATE}%',
			'%{USER_EMAIL}%',
			'%{USER_LOGIN}%',
			'%{USER_NAME}%',
		];

		/** @var array 메일 전송시 본문과 제목에서 허용하는 치환 코드의 짝 */
		private $substitutes = [];

		public function __construct() {
			$this->substitutes = [
				wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES ),
				self::DORMANT_LIMIT_MONTH,
				esc_url( wp_login_url() ),
				esc_url( wp_lostpassword_url() ),
				esc_url( site_url() ),
				esc_url( home_url() ),
				// 06: due date
				'',
				// 07: user_email
				'',
				// 08: user_login
				'',
				// 09: user_name
				'',
			];
		}

		/**
		 * 초기화 메소드
		 *
		 * @param string $main 플러그인 메인 파일. 활성화 훅 등록에 필요하다.
		 */
		public function init( $main ) {
			register_activation_hook( $main, [ $this, 'handle_activation' ] );
			register_deactivation_hook( $main, [ $this, 'handle_deactivation' ] );

			add_action( static::DORMANT_ACTION, [ $this, 'handle_cron' ] );
			add_action( 'update_option_shoplic_member_dormant_option', [ $this, 'update_option' ], 10, 2 );
			add_filter( 'authenticate', [ $this, 'maybe_recover_user' ], 90, 1 );

			add_action( 'load-users_page_shoplic_member', [ $this, 'prepare_notices' ] );
		}

		/**
		 * 시간대를 추측한다. 최대한 워드프레스에서 작성한 환경에 맞추어 알아넨다.
		 *
		 * @return string
		 */
		public static function get_timezone() {
			$timezone = get_option( 'timezone_string', 'Asia/Seoul' );
			if ( $timezone ) {
				return $timezone;
			} else {
				$offset = intval( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );

				if ( $offset ) {
					$timezone = timezone_name_from_abbr( '', $offset );
					/** @link https://bugs.php.net/bug.php?id=44780 timezone_name_from_abbr bug fix */
					if ( false === $timezone ) {
						foreach ( timezone_abbreviations_list() as $cities ) {
							foreach ( $cities as $city ) {
								if ( $city['offset'] === $offset ) {
									return $city['timezone_id'];
								}
							}
						}
					}
				}

				if ( ! $timezone ) {
					$timezone = 'UTC';
				}
			}

			return $timezone;
		}

		/**
		 * @param WP_User  $user
		 * @param string   $subject
		 * @param string   $body
		 * @param DateTime $due_date
		 *
		 * @return bool|WP_Error
		 */
		public function send_dormant_mail( $user, $subject, $body, $due_date ) {
			if ( ! ( $user instanceof WP_User ) ) {
				return new WP_Error( 'error', '올바른 사용자가 아닙니다.' );
			}

			$this->substitutes[6] = date_i18n( get_option( 'date_format' ), $due_date->getTimestamp(), true );
			$this->substitutes[7] = $user->user_email;
			$this->substitutes[8] = $user->user_login;
			$this->substitutes[9] = $user->first_name;

			$subject   = str_replace( $this->magic_codes, $this->substitutes, $subject );
			$body      = str_replace( $this->magic_codes, $this->substitutes, $body );
			$recipient = $user->user_email;

			return wp_mail(
				$recipient,
				wp_kses_post( $subject ),
				wp_kses_post( wptexturize( wpautop( $body ) ) )
			);
		}

		/**
		 * 비활성화 통지 받을 사용자를 골라낸다.
		 *
		 * @param DateTime|null     $threshold 임계치 기준 일자. 이 기준 일자보다 더 전에 로그인한 사용자는 대상이 된다.
		 *                                     null 이면 자동으로 12개월 전으로 설정됨.
		 * @param array|string|null $role      사용자 역할 목록. null 이면 기본으로 '고객' 역할을 대상으로 한다.
		 *
		 * @return WP_User[] 비활성화 통지를 받을 사용자 목록
		 */
		public function get_users_to_be_noticed( $threshold = null, $role = null ) {
			try {
				if ( is_null( $threshold ) ) {
					$threshold = new DateTime( 'today midnight', new DateTimeZone( $this->get_timezone() ) );
					$threshold->sub( new DateInterval( 'P12M' ) );

				} else {
					$threshold = clone $threshold;
				}

				$before = $this->get_notice_before();
				if ( $before > 0 ) {
					$threshold->add( new DateInterval( 'P' . $before . 'D' ) );
				}
			} catch ( Exception $e ) {
				// do nothing.
			}

			if ( is_null( $role ) ) {
				$role = [ 'customer' ];
			} elseif ( is_string( $role ) ) {
				$role = (array) $role;
			}

			$query = new WP_User_Query(
				[
					'user_status' => '0',
					'role__in'    => $role,
					'count_total' => false,
					'orderby'     => 'user_email',
					'order'       => 'ASC',
					'meta_query'  => [
						'relation' => 'AND',
						[
							'key'     => 'sm_last_login',
							'value'   => $threshold->format( 'Y-m-d H:i:s' ),
							'type'    => 'DATETIME',
							'compare' => '<',
						],
						[
							'key'     => 'shoplic_member_dormant_threshold',
							'compare' => 'NOT EXISTS',
						],
					],
				]
			);

			return $query->get_results();
		}

		/**
		 * 비활성화 처리할 사용자를 골라낸다.
		 *
		 * @param DateTime|null     $threshold 임계치 기준 일자. 이 기준보다 더 전에 로그인한 사용자를 대상으로 한다.
		 *                                     null 이면 자동으로 12개월 전으로 설정됨.
		 * @param array|string|null $role      사용자 역할 목록. null 이면 기본으로 '고객' 역할을 대상으로 한다.
		 * @param int|null          $time      'shoplic_member_dormant_threshold' 기준값.
		 *                                     기준값보다 작은 값을 가진 사용자를 대상으로 한다.
		 *
		 * @return WP_User[] 비활성화 처리할 사용자 목록. 인자로 전달한 $threshold 도 기준 대상이 되지만,
		 *                   사용자의 커스텀 필드 shoplic_member_dormant_threshold 값이 현재 타임스탬프보다 과거어야
		 *                   선택된다.
		 */
		public function get_users_to_be_deactivated( $threshold = null, $role = null, $time = null ) {
			if ( is_null( $threshold ) ) {
				try {
					$threshold = new DateTime( 'today midnight', new DateTimeZone( $this->get_timezone() ) );
					$threshold->sub( new DateInterval( 'P12M' ) );
				} catch ( Exception $e ) {
					// do nothing.
				}
			}

			if ( is_null( $role ) ) {
				$role = [ 'customer' ];
			} elseif ( is_string( $role ) ) {
				$role = (array) $role;
			}

			if ( is_null( $time ) ) {
				$time = time();
			}

			$query = new WP_User_Query(
				[
					'user_status' => '0',
					'role__in'    => $role,
					'count_total' => false,
					'meta_query'  => [
						'relation' => 'AND',
						[
							'key'     => 'sm_last_login',
							'value'   => $threshold->format( 'Y-m-d H:i:s' ),
							'type'    => 'DATETIME',
							'compare' => '<',
						],
						[
							'key'     => 'shoplic_member_dormant_threshold',
							'value'   => $time,
							'compare' => '<',
							'type'    => 'NUMERIC',
						],
					],
				]
			);

			return $query->get_results();
		}

		/**
		 * 활성화 콜백
		 * - 테이블 생성
		 * - 이전에 옵션에서 휴면처리를 활성화 한 경우 크론 스케쥴 등록
		 *
		 * @callback
		 * @activation
		 */
		public function handle_activation() {
			$this->create_table();
			if ( $this->is_enabled() ) {
				$this->set_schedule();
			}
			wp_roles()->add_role( 'deactivated', '휴면 회원' );
		}

		/**
		 * 비활성화 콜백
		 * - 크론 스케쥴 등록 해제
		 *
		 * @callback
		 * @deactivation
		 */
		public function handle_deactivation() {
			$this->unset_schedule();
			wp_roles()->remove_role( 'deactivated' );
		}

		/**
		 * 크론 스케쥴 콜백
		 * - 회뭔 비활성화 (휴면 처리)
		 * - 회원 휴면 처리 공지
		 * - 처리 내역 기록
		 * - 기록 정리
		 *
		 * @callback
		 * @action      shoplic_member_dormant_user
		 */
		public function handle_cron() {
			global $wpdb;

			$target_role = static::get_roles();

			if ( is_multisite() || ! $this->is_enabled() || empty( $target_role ) ) {
				return;
			}

			try {
				$threshold = new DateTime( 'today midnight', new DateTimeZone( $this->get_timezone() ) );
				$threshold->sub( new DateInterval( 'P12M' ) );
			} catch ( Exception $e ) {
				return;
			}

			$now = time();

			$deactivated = [
				'success' => [],
				'error'   => [],
			];

			$notified = [
				'success' => [],
				'error'   => [],
			];

			$content_type = function () {
				return 'text/html';
			};

			add_filter( 'wp_mail_content_type', $content_type );

			set_time_limit( 0 );

			$users_to_be_deactivated = $this->get_users_to_be_deactivated( $threshold, $target_role, $now );
			foreach ( $users_to_be_deactivated as $user ) {
				$result = $this->send_dormant_mail(
					$user,
					$this->get_deactivation_subject(),
					$this->get_deactivation_body(),
					$this->get_due_date( $user )
				);
				if ( $result ) {
					$deactivated['success'][] = $user->ID;
					$this->deactivate_user( $user->ID, $now );
				} else {
					$deactivated['error'][] = $user->ID;
				}
			}

			/**
			 * 5.2.3 버전에서 User Query 시 같은 사용자가 중복되는 버그가 있음.
			 *
			 * 5~6년전 버그 같은데 왜 안 고쳐졌는지는 의문.
			 *
			 * @link https://core.trac.wordpress.org/ticket/17582
			 *
			 * @param WP_User_Query $user_query
			 */
			$pre_user_query = function ( WP_User_Query &$user_query ) {
				global $wpdb;
				$user_query->query_orderby = "GROUP BY {$wpdb->users}.ID ORDER BY {$wpdb->users}.user_email ASC";
			};
			add_action( 'pre_user_query', $pre_user_query );

			$users_to_be_notified = $this->get_users_to_be_noticed( $threshold, $target_role );
			foreach ( $users_to_be_notified as $user ) {
				$due_date = $this->get_due_date( $user );
				$result   = $this->send_dormant_mail(
					$user,
					$this->get_notification_subject(),
					$this->get_notification_body(),
					$due_date
				);
				if ( $result ) {
					$notified['success'][] = $user->ID;
					update_user_meta( $user->ID, 'shoplic_member_dormant_threshold', $due_date->getTimestamp() );
				} else {
					$notified['error'][] = $user->ID;
				}
			}

			remove_action( 'pre_user_query', $pre_user_query );

			remove_filter( 'wp_mail_content_type', $content_type );

			$deactivated_success = implode( ',', $deactivated['success'] );
			$deactivated_error   = implode( ',', $deactivated['error'] );
			$notified_success    = implode( ',', $notified['success'] );
			$notified_error      = implode( ',', $notified['error'] );

			$wpdb->query(
				$wpdb->prepare(
					"INSERT INTO {$wpdb->prefix}dormant_history
                     (created, notified_success, notified_error, deactivated_success, deactivated_error)
                     VALUES (%s, %s, %s, %s, %s)",
					gmdate( 'Y-m-d H:i:s', $now ),
					( $notified_success ? ( ',' . $notified_success . ',' ) : '' ),
					( $notified_error ? ( ',' . $notified_error . ',' ) : '' ),
					( $deactivated_success ? ( ',' . $deactivated_success . ',' ) : '' ),
					( $deactivated_error ? ( ',' . $deactivated_error . ',' ) : '' )
				)
			);

			$wpdb->query(
				$wpdb->prepare(
					"DELETE FROM {$wpdb->prefix}dormant_history
					WHERE created < %s",
					gmdate( 'Y-m-d H:i:s', $now - ( 30 * DAY_IN_SECONDS ) )
				)
			);
		}

		/**
		 * 옵션 저장시 콜백
		 * 활성화 처리시 크론 스케쥴 등록 처리.
		 * 비활성화 처리시 크론 스케쥴 취소 처리.
		 *
		 * @callback
		 * @action   update_option_shoplic_member_dormant_option
		 *
		 * @param array $old_value
		 * @param array $new_value
		 */
		public function update_option( $old_value, $new_value ) {
			if ( $old_value === $new_value ) {
				return;
			}

			if ( filter_var( $new_value['enabled'] ?? false, FILTER_VALIDATE_BOOLEAN ) ) {
				$cron_at = intval( $new_value['cron_at'] ?? - 1 );
				if ( false !== $cron_at && 0 <= $cron_at && $cron_at < 24 ) {
					$this->set_schedule( $cron_at );
				}
			} else {
				$this->unset_schedule();
			}
		}

		/**
		 * 로그인시 콜백.
		 * - 휴면 처리된 회원은 복구.
		 * - 휴면 통지를 받고 아직 처리되지 않은 회원은 휴면 처리를 초기화.
		 *
		 * @callback
		 * @filter     authenticate
		 *
		 * @param WP_User|WP_Error $user
		 *
		 * @return WP_User
		 */
		public function maybe_recover_user( $user ) {
			if ( $user instanceof WP_User ) {
				if ( '-1' == $user->user_status ) {
					$this->recover_user( $user->ID );
					$user = get_user_by( 'ID', $user->ID );
				}
				delete_user_meta( $user->ID, 'shoplic_member_dormant_threshold' );
			}

			return $user;
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return bool
		 */
		public static function is_enabled() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return filter_var( $option['enabled'] ?? false, FILTER_VALIDATE_BOOLEAN );
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return array
		 */
		public static function get_roles() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return array_filter( array_map( 'sanitize_key', $option['roles'] ?? [] ) );
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return int
		 */
		public static function get_notice_before() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return intval( $option['notice_before'] ?? 30 );
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return int
		 */
		public static function get_cron_at() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return $option['cron_at'] ?? 4;
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return string
		 */
		public static function get_deactivation_subject() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return $option['dormant_mail_title'] ?? '';
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return string
		 */
		public static function get_deactivation_body() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return $option['dormant_mail_content'] ?? '';
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return string
		 */
		public static function get_notification_subject() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return $option['notice_mail_title'] ?? '';
		}

		/**
		 * 옵션 래핑 메소드
		 *
		 * @return string
		 */
		public static function get_notification_body() {
			$option = get_option( 'shoplic_member_dormant_option', [] );
			return $option['notice_mail_content'] ?? '';
		}

		/**
		 * 사용자의 휴면일자를 리턴
		 *
		 * @param WP_User $user
		 *
		 * @return bool|DateTime
		 */
		public static function get_due_date( WP_User $user ) {
			$due_date = get_user_meta( $user->ID, 'sm_last_login', true );

			if ( $due_date ) {
				try {
					$tz       = new DateTimeZone( static::get_timezone() );
					$now      = new DateTime( 'now', $tz );
					$due_date = new DateTime( $due_date, $tz );
					$before   = self::get_notice_before();

					$due_date->setTime( 0, 0, 0 );
					$due_date->add( new DateInterval( 'P12M' ) );

					// 만약 휴면일지가 과거이면 오늘 자정에서 고지 일자 후로 조정한다.
					if ( $due_date <= $now && $before > 0 ) {
						$due_date = clone $now;
						$due_date->setTime( 0, 0, 0 );
						$due_date->add( new DateInterval( "P{$before}D" ) );
					}
				} catch ( Exception $e ) {
					return false;
				}

				return $due_date;
			}

			return false;
		}

		/**
		 * 알림메시지를 준비
		 *
		 * @callback
		 * @action    load-users_page_shoplic_member
		 *
		 * @see       Dormant_User::init()
		 */
		public function prepare_notices() {
			if ( 'dormant' === ( $_GET['tab'] ?? '' ) ) {
				if ( ! $this->get_timezone() ) {
					add_action( 'admin_notices', [ $this, 'timezone_notice' ] );
				}
				if ( is_multisite() ) {
					add_action( 'admin_notices', [ $this, 'multisite_notice' ] );
				}
			}
		}

		/**
		 * 타임존 설정 알림
		 *
		 * @callback
		 * @action      admin_notices
		 */
		public function timezone_notice() {
			echo '<div class="notice notice-error"><p>';
			echo '쇼플릭 멤버 휴면회원 기능 알림: 시간대 설정이 올바르지 않습니다.';
			echo '휴면회원 기능 동작이 올바르게 동작하기 위해서는 <a href="/wp-admin/options-general.php#timezone_string">일반 설정 &gt; 시간대</a>로 이동하여 시간대를 설정해 주시기 바랍니다.';
			echo '</p></div>';
		}

		/**
		 * 멀티사이트 설정 알림
		 *
		 * @callback
		 * @action      admin_notices
		 */
		public function multisite_notice() {
			echo '<div class="notice notice-error"><p>';
			echo '쇼플릭 멤버 휴면회원 기능은 멀티사이트를 지원하지 않습니다.';
			echo '</p></div>';
		}

		public function get_history( $args = [] ) {
			global $wpdb;

			$results = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT * FROM {$wpdb->prefix}dormant_history WHERE created >= %s ORDER BY created DESC",
					gmdate( 'Y-m-d 00:00:00', time() - DAY_7 )
				)
			);

			foreach ( $results as &$result ) {
				$result->created = new DateTime( $result->created, new DateTimeZone( 'UTC' ) );
				$result->created->setTimezone( new DateTimeZone( static::get_timezone() ) );

				$result->notified_success = array_filter(
					array_map( 'intval', explode( ',', $result->notified_success ) )
				);

				$result->notified_error = array_filter(
					array_map( 'intval', explode( ',', $result->notified_error ) )
				);

				$result->deactivated_success = array_filter(
					array_map( 'intval', explode( ',', $result->deactivated_success ) )
				);

				$result->deactivated_error = array_filter(
					array_map( 'intval', explode( ',', $result->deactivated_error ) )
				);
			}

			return $results;
		}

		/**
		 * 휴면회원 관리에 필요한 테이블 생성
		 */
		protected function create_table() {
			global $wpdb;

			if ( ! function_exists( 'dbDelta' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			}

			$query = [
				sprintf(
					'CREATE TABLE IF NOT EXISTS %1$s (
						id       BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
						user_id  BIGINT(20) UNSIGNED NOT NULL                            COMMENT \'사용자 ID. 유일값.\',
						created  DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT \'작성 시각. UTC.\',
						backup   LONGTEXT            NOT NULL                            COMMENT \'JSON 인코드된 데이터.\',
						PRIMARY KEY (id),
						UNIQUE KEY user_id_unique (user_id)
					) DEFAULT CHARSET=%2$s COLLATE=%3$s',
					$wpdb->prefix . 'dormant_users',
					$wpdb->charset,
					$wpdb->collate
				),
				sprintf(
					'CREATE TABLE IF NOT EXISTS %1$s (
						id 		            BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
						created             DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'실행 일시. UTC.\',
						notified_success    TEXT                NOT NULL                           COMMENT \'통지메일 발송 성공한 사용자 ID 목록\', 
						notified_error      TEXT                NOT NULL                           COMMENT \'통지메일 발송 실패한 사용자 ID 목록\',
						deactivated_success TEXT                NOT NULL                           COMMENT \'비활성화 메일 발송 성공한 사용자 ID 목록\',
						deactivated_error   TEXT                NOT NULL                           COMMENT \'비활성화 메일 발송 실패한 사용자 ID 목록\',
						PRIMARY KEY (id),
				        INDEX index_create (created)
					) DEFAULT CHARSET=%2$s COLLATE=%3$s',
					$wpdb->prefix . 'dormant_history',
					$wpdb->charset,
					$wpdb->collate
				),
			];

			dbDelta( $query );
		}

		/**
		 * 크론 일일 스케쥴 설정.
		 *
		 * @param int|null $cron_at 지정하지 않으면 옵션에서 가져와 설정한다.
		 */
		protected function set_schedule( $cron_at = null ) {
			$this->unset_schedule();;

			try {
				if ( is_null( $cron_at ) ) {
					$cron_at = $this->get_cron_at();
				}
				$tomorrow = new DateTime( 'tomorrow midnight', new DateTimeZone( $this->get_timezone() ) );
				wp_schedule_event(
					$tomorrow->getTimestamp() + $cron_at * HOUR_IN_SECONDS,
					'daily',
					static::DORMANT_ACTION
				);
			} catch ( Exception $e ) {
				wp_die( $e->getMessage() );
			}
		}

		/**
		 * 크론에서 스케쥴 해제
		 */
		protected function unset_schedule() {
			$timestamp = wp_next_scheduled( static::DORMANT_ACTION );
			if ( $timestamp ) {
				wp_unschedule_event( $timestamp, static::DORMANT_ACTION );
			}
		}

		/**
		 * 사용자를 비활성화 시킨다.
		 *
		 * 다음 작업을 한다.
		 * 1. users 테이블의 ID, user_login, user_pass, user_email 필드는 남긴다.
		 *    user_status 필드를 -1로 변경한다.
		 *    user_registered 필드는 0000-00-00 00:00:00 으로 변경한다.
		 * 2. usermeta 테이블의 모든 사용자 커스텀 필드를 삭제한다.
		 * 3. user, usermeta 에서 변경되거나 삭제한 필드 값은 모두 dormant_users 테이블에 기록한다.
		 * 4. 사용자 역할을 deactivated 로 변경한다.
		 *    usermeta 에 shoplic_member_dormant_deactivated 커스텀 필드를 붙인다. 값은 비활성화된 시각의 타임스탬프.
		 *
		 * @param int  $user_id
		 * @param null $time
		 *
		 * @return bool
		 */
		protected function deactivate_user( $user_id, $time = null ) {
			global $wpdb;

			if ( is_multisite() ) {
				return false;
			}

			if ( is_null( $time ) ) {
				$time = time();
			}

			$count = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT COUNT(*) FROM {$wpdb->prefix}dormant_users WHERE user_id = %d LIMIT 0, 1",
					$user_id
				)
			);

			if ( $count != 0 ) {
				return false;
			}

			delete_user_meta( $user_id, 'shoplic_member_dormant_threshold' );

			$user = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT user_nicename, user_url, user_registered, user_activation_key, display_name FROM {$wpdb->users} WHERE ID = %d",
					$user_id
				)
			);

			$usermeta = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT meta_key, meta_value FROM {$wpdb->usermeta} WHERE user_id = %d",
					$user_id
				)
			);

			$backup = [
				'user'     => [
					'user_nicename'       => $user->user_nicename,
					'user_url'            => $user->user_url,
					'user_registered'     => $user->user_registered,
					'user_activation_key' => $user->user_activation_key,
					'display_name'        => $user->display_name,
				],
				'usermeta' => [],
			];

			foreach ( $usermeta as $row ) {
				$backup['usermeta'][] = array(
					'key'   => $row->meta_key,
					'value' => $row->meta_value,
				);
			}

			$wpdb->query(
				$wpdb->prepare(
					"INSERT INTO {$wpdb->prefix}dormant_users (user_id, created, backup) VALUES (%d, %s, %s)",
					$user_id,
					gmdate( 'Y-m-d H:i:s', $time ),
					wp_json_encode( $backup )
				)
			);

			$wpdb->query(
				$wpdb->prepare(
					"UPDATE {$wpdb->users} SET user_nicename='', user_url='', user_registered='0000-00-00 00:00:00', user_activation_key='', user_status='-1', display_name='' WHERE ID = %d",
					$user_id
				)
			);

			$wpdb->query(
				$wpdb->prepare(
					"DELETE FROM {$wpdb->usermeta} WHERE user_id = %d",
					$user_id
				)
			);

			update_user_meta( $user_id, 'shoplic_member_dormant_deactivated', $time );
			update_user_meta( $user_id, $wpdb->prefix . 'capabilities', [ 'deactivated' => true ] );

			wp_cache_delete( $user_id, 'users' );
			wp_cache_delete( $user_id, 'user_meta' );

			return true;
		}

		/**
		 * 백업된 사용자를 복구한다.
		 *
		 * @param int $user_id
		 *
		 * @return bool
		 */
		protected function recover_user( $user_id ) {
			global $wpdb;

			if ( is_multisite() ) {
				return false;
			}

			// 상태 체크
			$user_status = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT user_status FROM {$wpdb->users} WHERE ID = %d",
					$user_id
				)
			);

			if ( is_null( $user_status ) || '-1' !== $user_status ) {
				return false;
			}

			// 백업 정보 쿼리
			$result = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT id, backup FROM {$wpdb->prefix}dormant_users WHERE user_id = %d",
					$user_id
				)
			);

			if ( ! $result ) {
				return false;
			}

			$backup = json_decode( $result->backup, true );

			if ( ! $backup ) {
				return false;
			}

			// 유저 테이블 갱신
			$wpdb->query(
				$wpdb->prepare(
					"UPDATE {$wpdb->users} SET
						 user_nicename = %s,
						 user_url = %s,
						 user_registered = %s,
						 user_activation_key = %s,
						 user_status = 0,
						 display_name = %s
					 WHERE ID = %d",
					$backup['user']['user_nicename'] ?? '',
					$backup['user']['user_url'] ?? '',
					$backup['user']['user_registered'] ?? '',
					$backup['user']['user_activation_key'] ?? '',
					$backup['user']['display_name'] ?? '',
					$user_id
				)
			);

			$last_login = get_user_meta( $user_id, 'sm_last_login', true );

			// 기존의 메타 필드 정보 소거
			$wpdb->query(
				$wpdb->prepare(
					"DELETE FROM {$wpdb->usermeta} WHERE user_id = %d",
					$user_id
				)
			);

			// 유저 메타 테이블 갱신
			$values = [];
			foreach ( $backup['usermeta'] ?? [] as $meta ) {
				if ( isset( $meta['key'] ) && isset( $meta['value'] ) ) {
					$values[] = $wpdb->prepare( '(%d, %s, %s)', $user_id, $meta['key'], $meta['value'] );
				}
			}
			$wpdb->query(
				"INSERT INTO {$wpdb->prefix}usermeta (user_id, meta_key, meta_value) VALUES " . implode( ',', $values )
			);

			if ( $last_login ) {
				update_user_meta( $user_id, 'sm_last_login', $last_login );
			}

			// 백업 정보 삭제
			$wpdb->query(
				$wpdb->prepare(
					"DELETE FROM {$wpdb->prefix}dormant_users WHERE id = %d",
					$result->id
				)
			);

			// 캐시 삭제. 다시 이 사용자의 정보 조회시 새로 불러오도록.
			wp_cache_delete( $user_id, 'users' );
			wp_cache_delete( $user_id, 'user_meta' );

			return true;
		}
	}

endif;

return new Dormant_User();
