<?php

namespace shoplic_member;

use DateTime;
use DateTimeZone;
use WP_Error;

if (!defined('ABSPATH')) {
  exit;
} // Exit if accessed directly

class Controller extends \Shoplic_Controller
{
  protected $id = 'shoplic_member';

  /** @var Dormant_User */
  private $dormant_user;

  function __construct($file)
  {
    parent::__construct($file);

    /**
     * 생성자에서는 module 불러오기가 안되어 저수준의 require 구문을 사용해야 한다.
     * 활성화, 비활성화 콜백 때문에 일찍 선언된다.
     *
     * @noinspection PhpIncludeInspection
     */
    $this->dormant_user = require dirname($file) . '/includes/dormant-users.php';
    $this->dormant_user->init(SHOPLIC_MEMBER_MAIN);
  }

  function init()
  {
    $this->add_filter('register_url');
    $this->add_filter('login_url');
    $this->add_filter('login_redirect');
    $this->add_filter('fields');
    $this->add_ajax_nopriv('register');
    $this->add_ajax_priv('profile');
    $this->add_ajax_priv('password');

    add_action('wp_ajax_request_dormant_mail_test', [$this, 'response_dormant_mail_test']);
  }

  function register_url($url = '')
  {
    $option = $this->get_option('shoplic_member_register_option');
    $url    = get_permalink($option->page);

    return $url;
  }

  function login_url($url = '')
  {
    $option = $this->get_option('shoplic_member_login_option');
    $url    = get_permalink($option->page);

    return $url;
  }

  function login_redirect($redirect = '')
  {
    if (empty($redirect)) {
      $option   = $this->get_option('shoplic_member_login_option');
      $redirect = get_permalink($option->redirect);
    }

    return $redirect;
  }

  function fields($default_fields = [])
  {
    if (empty($default_fields)) {
      $default_fields = $this->conf('fields');
    }

    $fields_option = get_option('shoplic_member_fields_option');
    if ($fields_option === false) {
      return $default_fields;
    }

    // print_r($fields_option);
    // print_r($default_fields);
    // return wp_parse_args($fields_option, $default_fields);

    $fields = [];
    // foreach ($default_fields as $key => $default) {
    foreach ($fields_option as $key => $value) {
      // $fields[$key] = isset($fields_option[$key]) ? wp_parse_args($fields_option[$key], $default) : $default;
      $fields[$key] = wp_parse_args($value, $default_fields[$key]);
      // $field['title'] = $default_fields[$id]['title'];
      // $field['type'] = $default_fields[$id]['type'];
      // if (isset($default_fields[$id]['inputmask']))
      //     $field['inputmask'] = $default_fields[$id]['inputmask'];
      // $fields[$id] = $field;
    }

    return $fields;
  }

  function register()
  {
    $this->do_action('before_register');

    $fields = apply_filters('shoplic_register_fields', $this->fields(), 'insert');

    $errors   = [];
    $data     = [];
    $usermeta = [];
    foreach ($fields as $key => $field) {
      if (!isset($field['enabled'])) {
        continue;
      }

      $value = $this->post($key, isset($field['value']) ? $field['value'] : '');
      if ($field['required'] && empty($value)) {
        $errors[$key] = '필수입력 항목입니다.';
      } else {
        switch ($key) {
          case 'user_login':
            $data[$key] = sanitize_user($value);
            break;
          case 'user_email':
            $data[$key] = sanitize_email($value);
            break;
          case 'user_pass':
          case 'confirm_pass':
            $data[$key] = $value;
            break;
          default:
            $data[$key] = $usermeta[$key] = sanitize_text_field($value);
            if ($key == 'first_name') {
              $usermeta['billing_first_name'] = $usermeta[$key];
            }
            break;
        }
      }
    }
    if (!empty($errors)) {
      wp_send_json_error($errors);
    }

    if (username_exists($data['user_login'])) {
      $errors['user_login'] = sprintf('사용하실 수 없는 %s입니다.', $fields['user_login']['label']);
    }

    if (!validate_username($data['user_login'])) {
      $errors['user_login'] = sprintf('유효하지 않은 %s입니다.', $fields['user_login']['label']);
    }

    if (email_exists($data['user_email'])) {
      $errors['user_email'] = sprintf('사용하실 수 없는 %s입니다.', $fields['user_email']['label']);
    }

    if (!is_email($data['user_email'])) {
      $errors['user_email'] = sprintf('유효하지 않은 %s입니다.', $fields['user_email']['label']);
    }

    if (isset($data['confirm_pass']) && $data['confirm_pass'] !== $data['user_pass']) {
      $errors['confirm_pass'] = '비밀번호가 일치하지 않습니다.';
    }

    // Google reCaptcha
    $option = $this->get_option('shoplic_member_register_option');
    if ($option->recaptcha == 'v3' && $option->recaptcha_site_key != '' && $option->recaptcha_secret_key != '') {
      if (!isset($_POST['recaptcha_token']) || empty($_POST['recaptcha_token'])) {
        $errors['register'] = '회원가입을 처리할 수 없습니다. 고객센터로 문의하십시오.';
      } else {
        $response = wp_remote_post(
          'https://www.google.com/recaptcha/api/siteverify',
          array(
            'method' => 'POST',
            'body'   => array(
              'secret'   => $option->recaptcha_secret_key,
              'response' => $_POST['recaptcha_token'],
            ),
          )
        );

        if (is_wp_error($response)) {
          $errors['register'] = '회원가입을 처리할 수 없습니다. 고객센터로 문의하십시오.(' . $response->get_error_message() . ').';
        } else {
          $json = json_decode($response['body'], true);
          if (!$json['success']) {
            $errors['register'] = '회원가입을 처리할 수 없습니다. 고객센터로 문의하십시오.(' . implode(
              ', ',
              $json['error-codes']
            ) . ')';
          } elseif ($json['score'] < 0.5) {
            $errors['register'] = '회원가입을 처리할 수 없습니다. 고객센터로 문의하십시오.(score: ' . $json->score . ')';
          } else {
            $usermeta['grecaptcha'] = $json;
          }
        }
      }
    }

    if (!empty($errors)) {
      wp_send_json_error($errors);
    }

    add_filter(
      'insert_user_meta',
      function ($meta, $user, $update) use ($usermeta) {
        return array_merge($usermeta, $meta);
      },
      10,
      3
    );
    $user_data = array_intersect_key(
      $data,
      array_flip(
        [
          'user_login',
          'user_pass',
          'user_email',
          'user_url',
          'user_nicename',
          'display_name',
          'user_registered',
          'first_name',
        ]
      )
    );
    $result    = wp_insert_user($user_data);

    if (is_wp_error($result)) {
      $message = $result->get_error_message();
      switch ($result->get_error_code()) {
        case 'user_login_too_long':
        case 'existing_user_login':
        case 'invalid_username':
          $errors['user_login'] = $message;
          break;
        case 'existing_user_email':
          $errors['user_email'] = $message;
          break;
      }
      if (!empty($errors)) {
        wp_send_json_error($errors);
      }
    } else {
      $option   = $this->get_option('shoplic_member_register_option');
      $redirect = $option->redirect != null ? get_permalink($option->redirect) : home_url();
      if ($option->logged_in != null) {
        wp_set_auth_cookie($result, false, false);
      }

      $data['ID'] = $result;
      $this->do_action('after_register', $data);

      wp_send_json_success($redirect);
    }
  }

  function profile()
  {
    $fields = $this->fields();
    // print_r($fields);

    $errors   = [];
    $data     = ['ID' => get_current_user_id()];
    $usermeta = [];
    foreach ($fields as $key => $field) {
      if (!isset($field['enabled']) || $key == 'user_login' || $field['type'] == 'password') {
        continue;
      }

      $value = $this->post($key);
      if ($field['required'] && empty($value)) {
        $errors[$key] = '필수입력 항목입니다.';
      } else {
        switch ($key) {
          case 'user_email':
            $data[$key] = sanitize_email($value);
            break;
          default:
            $data[$key] = $usermeta[$key] = sanitize_text_field($value);
            break;
        }
      }
    }
    if (!empty($errors)) {
      wp_send_json_error($errors);
    }

    $user = wp_get_current_user();
    if ($user->user_email != $data['user_email'] && email_exists($data['user_email'])) {
      $errors['user_email'] = sprintf('사용하실 수 없는 %s입니다.', $fields['user_email']['label']);
    }

    if (!is_email($data['user_email'])) {
      $errors['user_email'] = sprintf('유효하지 않은 %s입니다.', $fields['user_email']['label']);
    }

    if (!empty($errors)) {
      wp_send_json_error($errors);
    }

    add_filter(
      'insert_user_meta',
      function ($meta, $user, $update) use ($usermeta) {
        $meta = array_merge($usermeta, $meta);
        return $meta;
      },
      10,
      3
    );
    $user_data = array_intersect_key(
      $data,
      array_flip(
        [
          'ID',
          'user_login',
          'user_pass',
          'user_email',
          'user_url',
          'user_nicename',
          'display_name',
          'user_registered',
          'first_name',
        ]
      )
    );
    $result    = wp_update_user($user_data);

    if (is_wp_error($result)) {
      $message = $result->get_error_message();
      switch ($result->get_error_code()) {
        case 'existing_user_email':
          $errors['user_email'] = $message;
          break;
      }
      if (!empty($errors)) {
        wp_send_json_error($errors);
      }
    }

    wp_send_json_success();
  }

  function password()
  {
    $old_pass = $this->post('old_pass');
    $user     = wp_get_current_user();
    if ($user && wp_check_password($old_pass, $user->data->user_pass, $user->ID)) {
      $user_pass    = $this->post('user_pass');
      $confirm_pass = $this->post('confirm_pass');
      if ($user_pass !== $confirm_pass) {
        wp_send_json_error('새 비밀번호가 일치하지 않습니다.');
      }
      // wp_set_password($user_pass, $user->ID);
      wp_update_user(array('ID' => $user->ID, 'user_pass' => $user_pass));
      wp_send_json_success();
    } else {
      wp_send_json_error('현재 비밀번호가 일치하지 않습니다.');
    }
  }

  function post($param, $default = '')
  {
    return isset($_POST[$param]) ? $_POST[$param] : $default;
  }

  /**
   * AJAX 요청 응답. 테스트 메일을 전송해 본다.
   *
   * @callback
   * @action  wp_ajax_request_dormant_mail_test
   *
   * @see     shoplic-member/components/shoplic-member/assets/js/dormant-page.js
   */
  public function response_dormant_mail_test()
  {
    if (!check_ajax_referer($this->get_tag('dormant-page'), 'nonce')) {
      wp_send_json_error(new WP_Error('error', 'Nonce failure.'));
    }

    $subject = sanitize_text_field($_REQUEST['subject'] ?? '');
    if (empty($subject)) {
      wp_send_json_error(new WP_Error('error', '제목이 비었습니다.'));
    }

    $body = wp_kses_post($_REQUEST['body'] ?? '');
    if (empty($body)) {
      wp_send_json_error(new WP_Error('error', '내용이 비었습니다.'));
    }

    $content_type = function () {
      return 'text/html';
    };

    add_filter('wp_mail_content_type', $content_type);
    $result = $this->dormant_user->send_dormant_mail(
      get_user_by('email', get_option('admin_email')),
      '[테스트] ' . $subject,
      "<p><strong>테스트를 위해 보낸 메일입니다.</strong></p>" . $body,
      new DateTime('tomorrow noon', new DateTimeZone(Dormant_User::get_timezone()))
    );
    remove_filter('wp_mail_content_type', $content_type);

    if (!$result) {
      wp_send_json_error(new WP_Error('error', '이메일 송신이 실패했습니다.'));
    }

    wp_send_json_success(['message' => '테스트 메일을 관리자 메일로 발송했습니다.']);
  }
};

return new Controller(__FILE__);
