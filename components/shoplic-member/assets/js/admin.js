jQuery(function($) {
    /*
    $('select.select2').select2({
        allowClear: true
    });
    */

    // Sorting
    $( 'table.shoplic-user-field-table tbody' ).sortable({
        items: 'tr',
        cursor: 'move',
        axis: 'y',
        handle: 'td.sort',
        scrollSensitivity: 40,
        helper: function( event, ui ) {
            ui.children().each( function() {
                $( this ).width( $( this ).width() );
            });
            ui.css( 'left', '0' );
            return ui;
        },
        start: function( event, ui ) {
            ui.item.css( 'background-color', '#f6f6f6' );
        },
        stop: function( event, ui ) {
            ui.item.removeAttr( 'style' );
        }
    });


});
