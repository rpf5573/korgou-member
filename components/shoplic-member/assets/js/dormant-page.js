(function ($) {
	var dormantPage = $.extend({
			nonce: '',
			textConfirmSend: '',
			textWaitAMoment: '잠시만 기다려 주세요.'
		}, window.hasOwnProperty('dormantPage') ? window.dormantPage : {}),
		ajaxUrl     = window.hasOwnProperty('ajaxurl') ? window.ajaxurl : '/wp-admin/admin-ajax.php';

	$('#notice-mail-test,#dormant-mail-test').on('click', function (e) {
		var subject,
			body,
			type    = e.target.dataset.type,
			message = null,
			$this   = $(this);
		if (confirm(dormantPage.textConfirmSend)) {
			if ('notice' === type) {
				subject = $('#sf-shoplic_member_dormant_option-notice_mail_title').val();
				body    = $('#shoplic_member_dormant_option-notice_mail_content').val();
			} else if ('dormant' === type) {
				subject = $('#sf-shoplic_member_dormant_option-dormant_mail_title').val();
				body    = $('#shoplic_member_dormant_option-dormant_mail_content').val();
			}
			$.ajax(ajaxUrl, {
				method: 'post',
				data: {
					action: 'request_dormant_mail_test',
					subject: subject,
					body: body,
					nonce: dormantPage.nonce
				},
				beforeSend: function () {
					message = $this.siblings('span.mail-test-message');
					if (!message.length) {
						message = $('<span class="mail-test-message" style="margin-left: 20px;"></span>').insertAfter($this);
					}
					message.text(dormantPage.textWaitAMoment);
					$this.hide();
				},
				success: function (response) {
					if (response.success) {
						if (response.hasOwnProperty('data')) {
							message.text(response.data.message);
						}
					} else {
						if (response.hasOwnProperty('data')) {
							message.text(response.data[0].message);
						}
					}
				},
				error: function (jqXHR, textStatus) {
					message.text(textStatus + ' ' + jqXHR.responseText);
				},
				complete: function () {
					$this.show();
				}
			});
		}
		e.preventDefault();
	});
})(jQuery);
