<?php
namespace shoplic_member\woocommerce;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Controller extends \Shoplic_Controller
{
    protected $id = 'woocommerce';

    function init()
    {
        // $this->add_action('login_form');
        $this->add_action('login_redirect', 10, 2);
    }

    function login_form()
    {
        echo '<br>';
    }

    function login_redirect($redirect = '', $user)
    {
        $wc_myaccount = remove_query_arg( 'wc_error', wc_get_page_permalink( 'myaccount' ));
        if ($redirect == $wc_myaccount)
            $recirect = '';

        return apply_filters('shoplic_member_login_redirect', $redirect);
    }
};

return new Controller(__FILE__);
