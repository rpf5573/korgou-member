<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'shoplic_restriction';

    static $META_KEY = '_shoplic_block';

    function init()
    {
        add_action('template_redirect', [$this, 'restrict']);
    }

    function restrict()
    {
        global $wp, $post;  
        if (is_singular()) {
            $block = get_post_meta($post->ID, self::$META_KEY, true);
            if ($block == '1' && !is_user_logged_in()) {
                // $current_url = home_url(add_query_arg(array(),$wp->request));
                // $current_url = home_url($wp->request);
                $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                $login_url = apply_filters('login_url', home_url(), $current_url);
                wp_redirect($login_url);
                die;
            }
        }
    }
};
