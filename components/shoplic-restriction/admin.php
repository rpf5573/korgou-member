<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return new class(__FILE__) extends Shoplic_Admin
{
    protected $id = 'shoplic_restriction';

    static $META_KEY = '_shoplic_block';

    function __construct($file)
    {
        parent::__construct($file);
    }

    function init()
    {
        add_action('add_meta_boxes', [$this, 'add_meta_boxes'], 10, 2);
		add_action('save_post', [$this, 'save_post']);
    }

    function add_meta_boxes($post_type, $post)
    {
        add_meta_box( $this->id . '_' . $post_type, '접근 권한', [$this, 'meta_box'], $post_type, 'side', 'high'
                    // ,array( '__back_compat_meta_box' => true, ) // @todo Convert to Block and declare this for backwards compat ONLY!
        );
    }
    
    function meta_box($post)
    {
        $block = get_post_meta($post->ID, self::$META_KEY, true);
        $this->view('meta_box', ['block' => $block]);
    }

    function save_post($post_id)
    {
        if (array_key_exists(self::$META_KEY, $_POST)) {
            $block = $_POST[self::$META_KEY];
            if ($block == '0') {
                delete_post_meta($post_id, self::$META_KEY);
            } else {
                update_post_meta(
                    $post_id,
                    self::$META_KEY,
                    $_POST[self::$META_KEY]
                );
            }
        }
    }
};