<?php
namespace shoplic_member_menu;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Controller extends \Shoplic_Controller
{
    protected $id = 'shoplic_member_menu';

    protected $logged_in;

    protected $login_page;

    protected $logout_page;

    function __construct($file)
    {
        parent::__construct($file);
    }

    function init()
    {
        if (is_admin())
            return;

        $this->logged_in = is_user_logged_in();
        $this->login_option = $this->get_option('shoplic_member_login_option');
        $this->logout_option = $this->get_option('shoplic_member_logout_option');

        add_filter('wp_setup_nav_menu_item', array($this, 'wp_setup_nav_menu_item'));
        add_filter('login_url', array($this, 'login_url'), 10, 3);
        add_action('template_redirect', array($this, 'logout_page'), 10000 );
    }

    function login_url( $login_url, $redirect = null, $force_reauth = null )
    {
        if (is_admin() || $this->login_option->page == null)
            return $login_url;

        if ($redirect == null) {
            if ($this->login_option->redirect == null || $this->login_option->redirect == '') {
                global $wp;
                $redirect = home_url( $wp->request );
            } else {
                $redirect = get_permalink($this->login_option->redirect);
            }
        }
        $url = get_permalink($this->login_option->page);
        $url = add_query_arg('redirect_to', urlencode($redirect), $url);
        return $url;
    }

    function wp_setup_nav_menu_item($item)
    {
        $sm_menu = get_post_meta($item->ID, 'sm_menu', true);
        if (isset($sm_menu['restriction'])) {
            $item->_invalid = ($sm_menu['restriction'] == '1' && $this->logged_in) || ($sm_menu['restriction'] == '2' && !$this->logged_in);
        }
        if ($item->_invalid === false && $item->object_id == $this->login_option->page) {
            $item->url = wp_login_url();
        }
        // print_r($item);

        return $item;
    }

    function logout_page()
    {
        global $post;

        // if ( $this->logout_option->page && is_page($this->logout_option->page)) {
        if ( is_singular('page') && $post->post_name == 'logout') {

            if ( is_user_logged_in() ) {

                if ( isset( $_REQUEST['redirect_to'] ) && $_REQUEST['redirect_to'] !== '' ) {
                    wp_logout();
                    session_unset();
                    exit( wp_redirect( $_REQUEST['redirect_to'] ) );
                } else if ( $this->logout_option->redirect != null && $this->logout_option->redirect != '') {
                    wp_logout();
                    session_unset();
                    exit( wp_redirect( get_permalink($this->logout_option->redirect) ) );
                } else {
                    wp_logout();
                    session_unset();
                }
            }
            exit( wp_redirect( home_url() ) );
        }
    }

};

return new Controller(__FILE__);
