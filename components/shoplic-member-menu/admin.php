<?php
namespace shoplic_member_menu;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Admin extends \Shoplic_Admin
{
    protected $id = 'shoplic_member_menu';

    function __construct($file)
    {
        parent::__construct($file);
    }

    function init()
    {
		add_action( 'wp_update_nav_menu_item', array( $this, 'save' ), 10, 3 );
		//add_filter( 'manage_nav-menus_columns', array( __CLASS__, '_columns' ), 99 );

		add_action( 'load-nav-menus.php', array( $this, 'load_nav_menu' ) );
		add_action( 'admin_footer-nav-menus.php', array( $this, 'load_template' ) );
    }


    /**
    * @param $menu_id
    * @param $menu_item_db_id
    * @param $menu_item_args
    */
    function save( $menu_id, $menu_item_db_id, $menu_item_args )
    {
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return;
        }

        if ( empty( $_POST['menu-item-db-id'] ) || ! in_array( $menu_item_db_id, $_POST['menu-item-db-id'] ) ) {
            return;
        }

        if (isset($_POST['menu-item-sm_menu'][$menu_item_db_id]))
            update_post_meta( $menu_item_db_id, 'sm_menu', $_POST['menu-item-sm_menu'][$menu_item_db_id] );
    }

    /**
    *
    */
    function load_nav_menu()
    {
        $this->script('nav-menu', ['deps'=>['jquery','wp-util']]);
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
    }

    /**
    *
    */
    function enqueue_scripts()
    {
        $menus = get_posts( 'post_type=nav_menu_item&numberposts=-1' );
        $menu_data = array();

        foreach ( $menus as $menu ) {
            $data = get_post_meta( $menu->ID, 'sm_menu', true );
            $menu_data[ $menu->ID ] = empty($data) ? [] : $data;
        }
        $this->localize('nav-menu', 'sm_menu_data', $menu_data );
    }

    /**
    *
    */
    function load_template()
    {
        $this->view('template');
    }

}

return new Admin(__FILE__);
