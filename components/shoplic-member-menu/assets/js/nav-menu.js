jQuery(function($) {
    var template = wp.template('sm-nav-menus-fields');

    $( 'ul#menu-to-edit > li' ).each( function(){
        var id = $(this).attr('id').substr(10);
        var data = sm_menu_data[id];
        data.menuItemID = id;
        var template_content = template(data);

        if ( $( this ).find( 'fieldset.field-move' ).length > 0 ) {
            $( this ).find( 'fieldset.field-move' ).before( template_content );
        } else {
            $( this ).find( '.menu-item-actions' ).before( template_content );
        }
    });

    $( document ).on( 'menu-item-added', function ( e, $menuMarkup ) {
        var id = $( $menuMarkup ).attr('id').substr(10);
        var data = {
            menuItemId: id
        }
        var template_content = template(data);

        if ( $( $menuMarkup ).find( 'fieldset.field-move' ).length > 0 ) {
            $( $menuMarkup ).find( 'fieldset.field-move' ).before( template_content );
        } else {
            $( $menuMarkup ).find( '.menu-item-actions' ).before( template_content );
        }
    });

});
