<script type="text/html" id="tmpl-sm-nav-menus-fields">
    <div class="sm-nav-edit">
        <p class="description description-wide">
            <label for="edit-menu-item-sm_menu_restriction-{{data.menuItemID}}">
                메뉴 보기 권한
            </label>
        </p>
        <p class1="description-wide">
            <input type="hidden" name="menu-item-sm_menu[{{data.menuItemID}}][enabled]" value="1">

            <label>
                <input type="radio" id="edit-menu-item-sm_menu_restriction-{{data.menuItemID}}" name="menu-item-sm_menu[{{data.menuItemID}}][restriction]" value="0"
                    <# if( data.restriction == '0' ){ #>checked<# } #> >누구나
            </label>
            <label style="margin-left: 8px;">
                <input type="radio" id="edit-menu-item-sm_menu_restriction-{{data.menuItemID}}" name="menu-item-sm_menu[{{data.menuItemID}}][restriction]" value="1"
                    <# if( data.restriction == '1' ){ #>checked<# } #> >로그아웃 사용자
            </label>
            <label style="margin-left: 8px;">
                <input type="radio" id="edit-menu-item-sm_menu_restriction-{{data.menuItemID}}" name="menu-item-sm_menu[{{data.menuItemID}}][restriction]" value="2"
                    <# if( data.restriction == '2' ){ #>checked<# } #> >로그인 사용자
            </label>
        </p>
    </div>
</script>
