<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'woocommerce_account';

    protected $endpoints;

    /*
    function __construct($file)
    {
        parent::__construct($file);

        $this->endpoints = [
            'profile' => '회원정보',
            'password' => '비밀번호',
        ];
    }

    function init()
    {
        $this->add_filter('menu_items');
        // $this->add_action('password_endpoint');
        foreach ($this->endpoints as $key => $title) {
            $this->add_action($key . '_endpoint');
            add_rewrite_endpoint($key, EP_ROOT | EP_PAGES);
        }

        add_filter('query_vars', [$this, 'query_vars']);
        add_filter('the_title', [$this, 'the_title']);
    }
    */

    function menu_items($items)
    {
        add_filter('woocommerce_locate_template', [$this, 'locate_template'], 10, 3);

        $logout_title = $items['customer-logout'];
        unset($items['customer-logout']);
        unset($items['edit-account']);
        unset($items['edit-address']);
        foreach ($this->endpoints as $key => $title) {
            $items[$key] = $title;
        }
        $items['customer-logout'] = $logout_title;

        return $items;
    }

    function query_vars($vars)
    {
        return array_merge($vars, $this->endpoints);
    }

    function the_title($title)
    {
        global $wp_query;
    	if ( ! is_admin() && is_main_query() && in_the_loop() && (function_exists('is_account_page') && is_account_page()) ) {
            foreach ($this->endpoints as $key => $_title) {
            	if ( isset( $wp_query->query_vars[$key] ) ) {
            		// New page title.
            		$title = $_title;
            	}
            }
        	remove_filter( 'the_title', [$this, 'the_title']);
        }
    	return $title;
    }

    function profile_endpoint()
    {
        do_action('shoplic_profile');
    }

    function password_endpoint()
    {
        do_action('shoplic_password');
    }

    function locate_template($template, $template_name, $template_path)
    {
        return ($template_name === 'myaccount/dashboard.php') ?
            $this->path . 'views/myaccount/dashboard.php' :
            $template;
    }
};

