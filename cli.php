<?php

use shoplic_member\Dormant_User;

class Shoplic_Member_CLI {
	public function __construct() {
	}

	public function recover_user( $args ) {
		$login = $args[0] ?? '';

		if ( $login ) {
			$user = get_user_by( 'login', $login );

			if ( ! $user ) {
				WP_CLI::error( '올바르지 않은 사용자입니다.' );
			}

			if ( ! user_can( $user, 'deactivated' ) ) {
				WP_CLI::error( '휴면 회원이 아닙니다.' );
			}

			/** @var Dormant_User $dormant */
			$dormant    = require __DIR__ . '/components/shoplic-member/includes/dormant-users.php';
			$reflection = new ReflectionClass( Dormant_User::class );
			$method     = $reflection->getMethod( 'recover_user' );

			$method->setAccessible( true );

			if ( $method->invoke( $dormant, $user->ID ) ) {
				WP_CLI::success( '사용자를 복구했습니다.' );
			} else {
				WP_CLI::error( '복구 중 에러 발생.' );
			}
		} else {
			WP_CLI::error( '사용자 로그인을 입력하세요.' );
		}
	}
}
