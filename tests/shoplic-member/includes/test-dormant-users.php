<?php

use shoplic_member\Dormant_User;

/**
 * Class Test_Dormant_User
 *
 * 휴면 회원 테스트 클래스
 *
 */
class Test_Dormant_User extends WP_UnitTestCase {
	/** @var Dormant_User */
	private static $instance;

	public static function setUpBeforeClass() {
		self::$instance = include dirname( SHOPLIC_MEMBER_MAIN ) .
		                          '/components/shoplic-member/includes/dormant-users.php';

		$method = self::makeMethodPublic( 'create_table' );
		$method->invoke( self::$instance );

		$option = [
			'enabled'              => 'yes',
			'cron_at'              => 2,
			'roles'                => [ 'customer' ],
			'dormant_mail_title'   => '휴면 처리 테스트 메일 제목',
			'dormant_mail_content' => "휴면 처리 테스트 메일 본문 - 치환 코드 치환 테스트
%{SITE_NAME}%
%{LIMIT_MONTH}%
%{DUE_DATE}%
%{USER_EMAIL}%
%{USER_LOGIN}%
%{USER_NAME}%
%{LOGIN_LINK}%
%{LOST_LINK}%
%{SITE_URL}%
%{HOME_URL}%",
			'notice_mail_title'    => '',
			'notice_mail_content'  => "휴면 통지 테스트 메일 본문 - 치환 코드 치환 테스트
%{SITE_NAME}%
%{LIMIT_MONTH}%
%{DUE_DATE}%
%{USER_EMAIL}%
%{USER_LOGIN}%
%{USER_NAME}%
%{LOGIN_LINK}%
%{LOST_LINK}%
%{SITE_URL}%",
		];

		update_option( 'shoplic_member_dormant_option', $option );
		update_option( 'timezone_string', 'Asia/Seoul' );
	}

	/**
	 * private, protected 메소드를 접근 가능하도록 일시 변경
	 *
	 * @param $methodName
	 *
	 * @return bool|ReflectionMethod
	 */
	public static function makeMethodPublic( $methodName ) {
		try {
			$reflection = new REflectionClass( Dormant_User::class );
			$method     = $reflection->getMethod( $methodName );
			$method->setAccessible( true );
			return $method;
		} catch ( Exception $e ) {
			return false;
		}
	}

	/**  */
	public function test_handle_cron() {
		global $wpdb;

		$yellow = new DateTime( 'today midnight', new DateTimeZone( static::$instance->get_timezone() ) );
		$yellow->sub( new DateInterval( 'P11M20D' ) );

		$red = new DateTime( 'today midnight', new DateTimeZone( static::$instance->get_timezone() ) );
		$red->sub( new DateInterval( 'P12M1D' ) );

		// 사용자 1: 휴면되지 않음
		$user1 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user1, 'sm_last_login', gmdate( 'Y-m-d H:i:s', time() - DAY_7 ) );

		// 사용자 2: 공지 대상
		$user2 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user2, 'sm_last_login', $yellow->format( 'Y-m-d H:i:s' ) );

		// 사용자 3: 휴면 대상
		$user3 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user3, 'sm_last_login', $red->format( 'Y-m-d H:i:s' ) );
		update_user_meta( $user3, 'shoplic_member_dormant_threshold', time() - DAY_1 );

		define( 'DOING_CRON', true );
		static::$instance->handle_cron();

		// 히스토리 테이블 점검
		$latest = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}dormant_history ORDER BY created DESC LIMIT 0, 1" );

		$notified_success = array_values( array_filter( explode( ',', $latest->notified_success ) ) );
		$this->assertEquals( 1, sizeof( $notified_success ) );
		$this->assertEquals( $user2, $notified_success[0] );

		$deactivated_success = array_values( array_filter( explode( ',', $latest->deactivated_success ) ) );
		$this->assertEquals( 1, sizeof( $deactivated_success ) );
		$this->assertEquals( $user3, $deactivated_success[0] );
	}

	/**
	 * 휴면 메일 공지 받을 사용자 목록 테스트
	 *
	 * @throws Exception
	 */
	public function test_get_users_to_be_noticed() {
		$last_login = new DateTime( 'today midnight', new DateTimeZone( static::$instance->get_timezone() ) );
		$last_login->sub( new DateInterval( 'P12M1D' ) );

		// 유저 1: 메일 공지를 받을 대상이 된다.
		$user1 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user1, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );

		// 유저 2: 메일 공지를 받지 않아야 한다. 이유: 고객 역할이 아님
		$user2 = $this->factory()->user->create( [ 'role' => 'editor' ] );
		update_user_meta( $user2, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );

		// 유저 3: 메일 공지를 받지 않아야 한다. 이유: 이미 공지를 받음
		$user3 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user3, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );
		update_user_meta( $user3, 'shoplic_member_dormant_threshold', time() + DAY_7 );

		// 유저 4: 메일 공지를 받지 않아야 한다. 이유: 이미 휴면 처리됨
		$user4           = $this->factory()->user->create( [ 'role' => 'customer' ] );
		$deactivate_user = $this->makeMethodPublic( 'deactivate_user' );
		$deactivate_user->invoke( static::$instance, $user4 );

		$users = static::$instance->get_users_to_be_noticed();
		$this->assertEquals( 1, sizeof( $users ) );
		$this->assertEquals( $user1, $users[0]->ID );
	}

	/**
	 * 비활성 처리 대상이 되는 사용자 목록 조회 테스트
	 *
	 * @throws Exception
	 */
	public function test_get_users_to_be_deactivated() {
		$last_login = new DateTime( 'today midnight', new DateTimeZone( static::$instance->get_timezone() ) );
		$last_login->sub( new DateInterval( 'P12M1D' ) );

		// 유저 1: 비활성화 대상이 된다.
		$user1 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user1, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );
		update_user_meta( $user1, 'shoplic_member_dormant_threshold', time() - DAY_1 );

		// 유저 2: 비활성화 되지 않는다. 이유: 고객 역할이 아님
		$user2 = $this->factory()->user->create( [ 'role' => 'editor' ] );
		update_user_meta( $user2, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );

		// 유저 3: 비활성화 되지 않는다. 이유: 아직 공지를 받지 않음.
		$user3 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user3, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );

		// 유저 4: 비활성화 되지 않는다. 이유: 이미 휴면 처리됨.
		$user4           = $this->factory()->user->create( [ 'role' => 'customer' ] );
		$deactivate_user = $this->makeMethodPublic( 'deactivate_user' );
		$deactivate_user->invoke( static::$instance, $user4 );

		// 유저 5: 비활성화 되지 않는다. 이유: 공지를 받있지만, 아직 최종 비활성화 시점을 넘지는 않았음.
		$user4 = $this->factory()->user->create( [ 'role' => 'customer' ] );
		update_user_meta( $user4, 'sm_last_login', $last_login->format( 'Y-m-d H:i:s' ) );
		update_user_meta( $user4, 'shoplic_member_dormant_threshold', time() + DAY_7 );

		$users = static::$instance->get_users_to_be_deactivated();
		$this->assertEquals( 1, sizeof( $users ) );
		$this->assertEquals( $user1, $users[0]->ID );
	}

	public function test_get_due_date() {
		$user = $this->factory()->user->create_and_get();

		update_user_meta( $user->ID, 'sm_last_login', '2019-01-01 03:15:22' );
		$this->assertEquals(
			new DateTime( '2020-01-01 00:00:00', new DateTimeZone( static::$instance->get_timezone() ) ),
			static::$instance->get_due_date( $user )
		);

		update_user_meta( $user->ID, 'sm_last_login', '2019-03-22 08:35:08' );
		$this->assertEquals(
			new DateTime( '2020-03-22 00:00:00', new DateTimeZone( static::$instance->get_timezone() ) ),
			static::$instance->get_due_date( $user )
		);
	}

	public function test_set_schedule() {
		$method        = self::makeMethodPublic( 'set_schedule' );
		$next_midnight = new DateTime( 'tomorrow midnight', new DateTimeZone( static::$instance->get_timezone() ) );

		$method->invoke( static::$instance );
		$this->assertEquals(
			$next_midnight->getTimestamp() + static::$instance->get_cron_at() * HOUR_IN_SECONDS,
			wp_next_scheduled( Dormant_User::DORMANT_ACTION )
		);

		$method->invoke( static::$instance, 6 );
		$this->assertEquals(
			$next_midnight->getTimestamp() + 6 * HOUR_IN_SECONDS,
			wp_next_scheduled( Dormant_User::DORMANT_ACTION )
		);
	}

	public function test_unset_schedule() {
		$result = wp_schedule_event( time() + 60, 'daily', Dormant_User::DORMANT_ACTION );
		$this->assertTrue( $result );

		$method = self::makeMethodPublic( 'unset_schedule' );
		$method->invoke( self::$instance );

		$this->assertFalse( wp_next_scheduled( Dormant_User::DORMANT_ACTION ) );
	}

	public function test_deactivate_user() {
		global $wpdb;

		$method            = $this->makeMethodPublic( 'deactivate_user' );
		$user              = $this->factory()->user->create_and_get(
			[
				'user_url' => 'http://example.com',
			]
		);
		$original_usermeta = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT meta_key AS 'key', meta_value AS 'value' FROM {$wpdb->usermeta} WHERE user_id = %d",
				$user->ID
			),
			ARRAY_A
		);
		$time              = time();

		$this->assertTrue( $method->invoke( static::$instance, $user->ID, $time ) );

		// 유저 테이블 체크
		$queried_user = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->users} WHERE ID = %d",
				$user->ID
			)
		);
		// 다음 필드는 건드리면 안 된다.
		$this->assertEquals( $user->ID, $queried_user->ID );
		$this->assertEquals( $user->user_login, $queried_user->user_login );
		$this->assertEquals( $user->user_pass, $queried_user->user_pass );
		$this->assertEquals( $user->user_email, $queried_user->user_email );
		// 이 필드는 지워져야 한다.
		$this->assertEmpty( $queried_user->user_nicename );
		$this->assertEmpty( $queried_user->user_url );
		$this->assertEmpty( $queried_user->user_activation_key );
		$this->assertEmpty( $queried_user->display_name );
		// 이 필드는 변경되어야 한다.
		$this->assertEquals( '0000-00-00 00:00:00', $queried_user->user_registered );
		$this->assertEquals( - 1, $queried_user->user_status );

		// check usermeta table
		$queried_usermeta = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT meta_key, meta_value FROM {$wpdb->usermeta} WHERE user_id = %d",
				$user->ID
			)
		);
		foreach ( $queried_usermeta as $meta ) {
			$meta_key   = $meta->meta_key;
			$meta_value = $meta->meta_value;
			if ( 'shoplic_member_dormant_deactivated' === $meta_key ) {
				$this->assertEquals( $time, $meta_value );
			} elseif ( $wpdb->prefix . 'capabilities' === $meta_key ) {
				$unserialized = maybe_unserialize( $meta_value );
				$this->assertArrayHasKey( 'deactivated', $unserialized );
				$this->assertTrue( $unserialized['deactivated'] );
			}
		}

		// check backup table
		$result = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT backup, created FROM {$wpdb->prefix}dormant_users WHERE user_id = %d",
				$user->ID
			)
		);
		$this->assertEquals( 1, sizeof( $result ) );

		$backup = json_decode( $result[0]->backup, true );
		$this->assertArrayHasKey( 'user', $backup );
		$this->assertEquals( $user->user_nicename, $backup['user']['user_nicename'] );
		$this->assertEquals( $user->user_url, $backup['user']['user_url'] );
		$this->assertEquals( $user->user_registered, $backup['user']['user_registered'] );
		$this->assertEquals( $user->user_activation_key, $backup['user']['user_activation_key'] );
		$this->assertEquals( $user->display_name, $backup['user']['display_name'] );

		$this->assertArrayHasKey( 'usermeta', $backup );
		$this->assertEquals( $original_usermeta, $backup['usermeta'] );

		$created = $result[0]->created;
		$this->assertEquals(
			$time,
			( new DateTime( $created, new DateTimeZone( 'UTC' ) ) )->getTimestamp()
		);

		// 중복된 요청은 불가능하도록 처리되어야 한다.
		$this->assertFalse( $method->invoke( static::$instance, $user->ID ) );
	}

	public function test_recover_user() {
		global $wpdb;

		$deactivate_user = $this->makeMethodPublic( 'deactivate_user' );
		$recover_user    = $this->makeMethodPublic( 'recover_user' );
		$user            = $this->factory()->user->create_and_get(
			[
				'user_url' => 'http://example.com',
			]
		);

		$this->assertTrue( $deactivate_user->invoke( static::$instance, $user->ID ) );
		$removed_meta_count = intval(
			$wpdb->get_var(
				$wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->usermeta} WHERE user_id = %d", $user->ID )
			)
		);

		$this->assertTrue( $recover_user->invoke( static::$instance, $user->ID ) );
		$recovered_meta_count = intval(
			$wpdb->get_var(
				$wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->usermeta} WHERE user_id = %d", $user->ID )
			)
		);

		// 유저메타의 키 수가 다름
		$this->assertTrue( $removed_meta_count <= $recovered_meta_count );

		$queried_user = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->users} WHERE ID = %d",
				$user->ID
			)
		);
		$this->assertEquals( $user->user_nicename, $queried_user->user_nicename );
		$this->assertEquals( $user->user_url, $queried_user->user_url );
		$this->assertEquals( $user->user_registered, $queried_user->user_registered );
		$this->assertEquals( $user->user_activation_key, $queried_user->user_activation_key );
		$this->assertEquals( 0, $queried_user->user_status );
		$this->assertEquals( $user->display_name, $queried_user->display_name );

		// 비활성화 마킹된 메타 키 삭제된 것 확인
		$this->assertEquals(
			0,
			$wpdb->get_var(
				$wpdb->prepare(
					"SELECT COUNT(*) FROM {$wpdb->usermeta} WHERE user_id = %d and meta_key = %s",
					$user->ID,
					'shoplic_member_dormant_deactivated'
				)
			)
		);
	}
}
